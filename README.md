## BuhUchet - БухУчет

### Русский

Программа предназначена просто потрениться на базах и на всем что окружает бахгалтерию.
Будет использован: C++ с фреймворком [Qt](https://www.qt.io/). Требуемый интерфейс на QML

### English

This program is just training for working with different types of DB. 
It will be written in C++ using framework [Qt](https://www.qt.io/). Needed interface will be written on QML